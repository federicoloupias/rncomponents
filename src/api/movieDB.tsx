import axios from "axios";


const movieDB = axios.create({
    baseURL: 'https://api.themoviedb.org/3/movie',
    params: {
        api_key: 'a37621f4ab6333c6c3910ba4443599d5',
        language : 'es-ES'
    }
});

export default movieDB;